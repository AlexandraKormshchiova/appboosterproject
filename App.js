/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StyleSheet, Button, View } from 'react-native';
import RNUssdDial from 'react-native-ussd-dial';

const App = () => {
  const getDeviceName = async () => {
    const deviceName = await RNUssdDial.getDeviceName();
    console.log('deviceName: ', deviceName);
  };

  return (
    <View style={styles.container}>
      <Button
        onPress={getDeviceName} // при нажатии на кнопку происходит вызов функции getDeviceName
        title="get device name"
        color="#841584"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
