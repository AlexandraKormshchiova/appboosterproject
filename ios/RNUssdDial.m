
#import "RNUssdDial.h"
#import <sys/utsname.h>

@interface DeviceUID : NSObject
+ (NSString *)syncUid;
+ (NSString *)uid;
@end

@implementation RNUssdDial

RCT_EXPORT_METHOD(getDeviceName:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  resolve([DeviceUID syncUid]);
}

- (NSString *) getDeviceName {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* deviceId = [NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
    #if TARGET_IPHONE_SIMULATOR
        deviceId = [NSString stringWithFormat:@"%s", getenv("SIMULATOR_MODEL_IDENTIFIER")];
    #endif
    return deviceId;
}

RCT_EXPORT_METHOD(getLog)
{
  struct utsname systemInfo;
  uname(&systemInfo);
  NSString* deviceId = [NSString stringWithCString:systemInfo.machine
                                          encoding:NSUTF8StringEncoding];

  NSLog(@"-------------------");
  NSLog(@"-------------------");
  NSLog(@"---------------Your name is %@", deviceId);
}

RCT_EXPORT_MODULE();

@end
